let paragraph = document.querySelectorAll("p");

for (let background of paragraph){
    background.style.backgroundColor = "#ff0000";
}

// ----------------------------------------

let id = document.getElementById("optionsList");
let parent = id.parentElement;
let child = id.childNodes;

console.log(id);
console.log(parent);
console.log(child);

// -------------------------------------------

let testParagraph = document.getElementById("testParagraph");
testParagraph.textContent = "This is a paragraph";

console.log(testParagraph);

// -------------------------------------------

let elements = document.querySelectorAll(".main-header *");

for (let newClass of elements){
    newClass.classList.add("nav-item");
}

console.log(elements);

// ---------------------------------------------

let elementsSection = document.querySelectorAll(".section-title");

elementsSection.forEach(removeClass => {
    removeClass.classList.remove("section-title");
});

console.log(elementsSection);
